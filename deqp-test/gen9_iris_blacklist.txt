dEQP-EGL.functional.buffer_age.no_preserve
dEQP-EGL.functional.query_context.simple.query_api
dEQP-EGL.functional.robustness.reset_context.fixed_function_pipeline.reset_status.index_buffer_out_of_bounds
dEQP-GLES31.functional.synchronization.inter_call.without_memory_barrier.ssbo_atomic_counter_mixed_dispatch_100_calls_1k_invocations

# flaky
dEQP-EGL.functional.color_clears.multi_context.gles1.rgba8888_pbuffer
dEQP-EGL.functional.color_clears.multi_thread.gles1_gles2_gles3.rgba8888_pbuffer
dEQP-EGL.functional.color_clears.multi_thread.gles2.rgb888_pbuffer
dEQP-EGL.functional.multithread.window
dEQP-EGL.functional.multithread.window_context
dEQP-GLES3.functional.fbo.color.repeated_clear.blit
dEQP-GLES3.functional.fbo.color.repeated_clear.sample.tex2d.rgba16i
