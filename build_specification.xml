<!--  Copyright (C) Intel Corp.  2014.  All Rights Reserved. -->

<!--  Permission is hereby granted, free of charge, to any person obtaining -->
<!--  a copy of this software and associated documentation files (the -->
<!--  "Software"), to deal in the Software without restriction, including -->
<!--  without limitation the rights to use, copy, modify, merge, publish, -->
<!--  distribute, sublicense, and/or sell copies of the Software, and to -->
<!--  permit persons to whom the Software is furnished to do so, subject to -->
<!--  the following conditions: -->

<!--  The above copyright notice and this permission notice (including the -->
<!--  next paragraph) shall be included in all copies or substantial -->
<!--  portions of the Software. -->

<!--  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, -->
<!--  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF -->
<!--  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. -->
<!--  IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE -->
<!--  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION -->
<!--  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION -->
<!--  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. -->

<!--   **********************************************************************/ -->
<!--   * Authors: -->
<!--   *   Mark Janes <mark.a.janes@intel.com> -->
<!--   **********************************************************************/ -->

<build_specification>
  <build_master host="otc-mesa-ci.jf.intel.com" results_dir="/mnt/jenkins/results"
                hostname="otc-mesa-ci"/>

  <!-- specified the dependency relationships between projects -->
  <projects>
    
    <!-- each project has a matching subdirectory with a build.py
         which automates the build.  -->

    <project name="android-buildtest" src_dir="mesa"/>

    <project name="sim-drm"/>

    <project name="fulsim">
      <prerequisite name="sim-drm" hardware="builder" arch="m64"/>
    </project>

    <project name="drm"/>

    <project name="mesa">
      <!-- options for a prerequisite are inherited unless overridden.
           For example, a build of mesa with arch=m32 will require a
           build of drm with arch=m32 -->
      <prerequisite name="drm"/>
    </project>

    <!-- only works on m64, due to llvm dependencies -->
    <project name="mesa-buildtest" src_dir="mesa">
      <prerequisite name="drm"/>
    </project>

    <project name="waffle"/>

    <!-- test projects should specify bisect_hardware and bisect_arch,
         which designate the full set of platforms that should be
         re-tested when a regression occurs.  See update_conf.py and
         bisect_project.py in scripts. -->
    <project name="deqp-test" src_dir="deqp"
             bisect_hardware="ilk,g33,g45,g965,bdw,hsw,ivb,snb,byt,bsw,gen9atom,gen9,gen9_iris,icl,icl_iris"
             bisect_arch="m64,m32">
      <!-- deqp-test will be specificed for a test platform (eg skl),
           but the hardware for its prerequisites should be a builder
           (since they are all compilation projects) -->
      <prerequisite name="deqp" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
    </project>

    <project name="deqp-runtime"
	    bisect_hardware="bdw,gen9,gen9atom"
	    bisect_arch="m32,m64">
      <prerequisite name="deqp" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
    </project>

    <project name="deqp">
      <prerequisite name="mesa"/>
    </project>

    <project name="glescts" src_dir="cts">
      <prerequisite name="mesa"/>
    </project>

    <project name="glcts">
      <prerequisite name="mesa"/>
    </project>

    <project name="glcts-test"
             bisect_hardware="snb,ivb,byt,hsw,bdw,bsw,gen9atom,gen9,gen9_iris,icl,icl_iris,bdw_iris"
             bisect_arch="m64,m32">
      <prerequisite name="glcts" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
    </project>

    <project name="glescts-test"
             bisect_hardware="g33,g45,ilk,g965,byt,bsw,bdw,hsw,snb,ivb,gen9atom,gen9,icl"
             bisect_arch="m64">
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="glescts" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
    </project>

    <project name="mi_builder-test">
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
    </project>

    <project name="glescts-full">
      <prerequisite name="glescts-test" hardware="bdw,gen9"/>
      <prerequisite name="glescts-test" hardware="hsw"/>
      <!-- only_for_type enforces the prerequisite when type matches
           the value. -->
      <prerequisite name="glescts-test" hardware="bsw,byt,gen9atom" only_for_type="daily"/>
      <prerequisite name="glescts-test" hardware="g33,g45,ilk,g965,ivb,snb,icl" only_for_type="daily"/>
      <prerequisite name="glescts-test"
                    only_for_type="daily"
                    arch="m32"
                    hardware="snb,ivb,hsw,bdw,gen9"/>
    </project>

    <project name="vkrunner"/>
      
    <project name="piglit">
      <prerequisite name="waffle" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="vkrunner" hardware="builder"/>
    </project>

    <project name="piglit-test"
             bisect_hardware="bdw,gen9,gen9_iris,hsw,ivb,snb,bsw,byt,g965,ilk,g33,g45,gen9atom,gen9atom_iris,icl,bdw_iris,icl_iris"
             bisect_arch="m64">
      <prerequisite name="piglit" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
    </project>

    <project name="piglit-test-internal"
             src_dir="mesa_ci_internal">
      <prerequisite name="piglit" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="sim-drm" hardware="builder" arch="m64"/>
      <prerequisite name="fulsim" arch="m64"/>
    </project>

    <project name="piglit-full">
      <prerequisite name="piglit-test"
                    hardware="snb,ivb,hsw,bdw,bdw_iris,gen9,gen9_iris"
                    shard="2"/>
      <prerequisite name="piglit-test"
                    hardware="icl,icl_iris"
                    shard="6"/>
      <prerequisite name="piglit-test"
                    only_for_type="daily"
                    hardware="snb,ivb,hsw,bdw,gen9,gen9_iris"
                    arch="m32"
                    shard="2"/>
      <prerequisite only_for_type="daily" name="piglit-test" hardware="bsw,byt,gen9atom,gen9atom_iris"
                    arch="m64,m32" shard="4"/>
      <prerequisite only_for_type="daily" name="piglit-test" hardware="ilk,g965,g45,g33"
                    arch="m64,m32"/>
    </project>
    
    <project name="crucible">
      <prerequisite name="mesa"/>
    </project>

    <project name="crucible-test"
             bisect_hardware="bdw,ivb,bsw,byt,hsw,gen9atom,gen9,icl"
             bisect_arch="m64">
      <prerequisite name="crucible" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
    </project>

    <project name="crucible-test-internal"
             bisect_hardware="tgl"
             bisect_arch="m64"
             src_dir="mesa_ci_internal">
      <prerequisite name="crucible" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
    </project>

    <project name="crucible-full">
      <prerequisite name="crucible-test" hardware="bdw,bsw,hsw,gen9atom,gen9"/>
      <prerequisite name="crucible-test" only_for_type="daily" arch="m64" hardware="gen9atom,icl"/>
    </project>

    <project name="skqp" />

    <project name="skqp-test"
             bisect_hardware="gen9atom,icl,icl_iris,gen9,gen9_iris,gen9atom_iris"
             bisect_arch="m64">
      <prerequisite name="waffle" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="skqp" hardware="builder"/>
    </project>

    <project name="skqp-full">
      <prerequisite name="skqp-test" hardware="gen9,gen9_iris,icl,icl_iris,bdw,bdw_iris" arch="m64"/>
      <prerequisite only_for_type="daily" name="skqp-test" hardware="gen9atom,gen9atom_iris" arch="m64"/>
    </project>

    <project name="vulkancts-test" src_dir="vulkancts"
             bisect_hardware="bdw,bsw,hsw,gen9atom,gen9,icl"
             bisect_arch="m64">
      <prerequisite name="vulkancts" hardware="builder"/>
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
    </project>

    <project name="vulkancts-full">
      <prerequisite name="vulkancts-test" hardware="gen9" shard="4"/>
      <prerequisite name="vulkancts-test" hardware="hsw,bdw" shard="6"/>
      <prerequisite name="vulkancts-test"
                    only_for_type="daily"
                    hardware="bdw,hsw,gen9"
                    arch="m32"
                    shard="4"/>
      <prerequisite name="vulkancts-test"
                    only_for_type="daily"
                    hardware="icl"
                    arch="m64"
                    shard="6"/>
      <prerequisite name="vulkancts-test"
                    only_for_type="daily"
                    hardware="bsw,gen9atom"
                    arch="m64"
                    shard="10"/>

      <prerequisite name="crucible-full"/>
    </project>

    <project name="mi_builder-full">
      <prerequisite name="mi_builder-test" hardware="ivb,hsw,bdw,gen9,icl"/>
    </project>

    <project name="vulkancts"/>

    <project name="all-test-vulkan">
      <prerequisite name="all-test"/>
      <prerequisite name="vulkancts-full"/>
    </project>
    
    <!-- all-test has no build.py, and exists only to provide
         dependency relationships. -->
    <project name="all-test">
      <prerequisite name="scons-buildtest" hardware="builder"/>
      <prerequisite name="piglit-full"/>
      <prerequisite name="deqp-full"/>
      <prerequisite name="mesa-buildtest" hardware="builder" arch="m64"/>
      <prerequisite name="android-buildtest" hardware="builder"/>
      <prerequisite name="glcts-full"/>
      <prerequisite name="glescts-full"/>
      <prerequisite name="skqp-full"/>
      <prerequisite name="webgl-full"/>
      <prerequisite name="mi_builder-full"/>
    </project>

    <project name="all-test-iris">
      <prerequisite name="piglit-test"
                    hardware="gen9_iris,bdw_iris"
                    shard="2"/>
      <prerequisite name="piglit-test"
                    hardware="icl_iris" shard="4"/>
      <prerequisite name="glcts-test"
                    hardware="gen9_iris,icl_iris,bdw_iris"/>
      <prerequisite name="skqp-test" hardware="gen9_iris,icl_iris" arch="m64"/>
    </project>

    <project name="test-single-arch-simdrm">
      <prerequisite name="test-single-arch-tgl-sim"/>
    </project>

    <project name="test-single-arch-tgl">
      <prerequisite name="piglit-test-internal" hardware="tgl" shard="8" arch="m64"/>
      <prerequisite name="crucible-test-internal" hardware="tgl" arch="m64"/>
    </project>

    <project name="test-single-arch-tgl-sim">
      <prerequisite name="piglit-test-internal" hardware="tgl_sim" shard="6" arch="m64"/>
    </project>
    

    <project name="test-single-arch-vulkan">
      <prerequisite name="test-single-arch"/>
      <prerequisite name="vulkancts-full"/>
    </project>

    <project name="test-single-arch">
      <prerequisite name="piglit-test" hardware="ilk,g45,g965"/>
      <prerequisite name="piglit-test" hardware="snb,ivb,hsw,bdw,gen9,gen9_iris" shard="2"/>
      <prerequisite only_for_type="daily" name="piglit-test" hardware="g33,bsw,byt,gen9atom,gen9atom_iris,icl" shard="4"/>
      <prerequisite name="scons-buildtest" hardware="builder" arch="m64"/>
      <prerequisite name="mesa-buildtest" hardware="builder" arch="m64"/>
      <prerequisite name="android-buildtest" hardware="builder"/>
      <prerequisite name="deqp-full"/>
      <prerequisite name="glcts-full"/>
      <prerequisite name="glescts-full"/>
      <prerequisite name="skqp-full"/>
      <prerequisite name="mi_builder-full"/>
      <prerequisite name="webgl-full"/>
    </project>

    <!-- this is used for Nanley's percheckin build -->
    <project name="test-single-arch-nchery">
      <prerequisite name="piglit-full"/>
      <prerequisite name="piglit-test" hardware="icl"/>
      <prerequisite name="scons-buildtest" hardware="builder" arch="m64"/>
      <prerequisite name="mesa-buildtest" hardware="builder" arch="m64"/>
      <prerequisite name="deqp-full"/>
      <prerequisite name="glcts-full"/>
      <prerequisite name="glescts-full"/>
    </project>

    <project name="deqp-full">
      <prerequisite name="deqp-test" hardware="hsw,bdw,snb,ivb,gen9,gen9_iris,icl,icl_iris" shard="5"/>
      <prerequisite name="deqp-test"
                    only_for_type="daily"
                    hardware="ilk,g33,g45,g965,byt,bsw,gen9atom" shard="4"/>
      <prerequisite name="deqp-test"
                    only_for_type="daily"
                    arch="m32"
                    hardware="snb,ivb,hsw,bdw,gen9atom,bsw,gen9" shard="4"/>
    </project>

    <project name="deqp-runtime-full">
      <prerequisite name="deqp-runtime" hardware="bdw,gen9" shard="5"/>
      <prerequisite name="deqp-runtime" hardware="gen9atom" shard="8"/>
    </project>

    <project name="glcts-full">
      <prerequisite name="glcts-test" hardware="snb,ivb,hsw,bdw,bdw_iris,gen9,gen9_iris" arch="m64"/>
      <prerequisite name="glcts-test" hardware="icl,icl_iris" shard="4"/>
      <prerequisite name="glcts-test" only_for_type="daily" hardware="hsw,bdw,gen9" arch="m32"/>
      <prerequisite name="glcts-test" only_for_type="daily" hardware="bsw,gen9atom,byt"/>
      <prerequisite name="glcts-test" only_for_type="daily" hardware="icl,icl_iris" arch="m32" shard="4"/>
    </project>

    <project name="webgl" bisect_hardware="gen9,gen9_iris">
      <prerequisite name="mesa" hardware="builder"/>
      <prerequisite name="waffle" hardware="builder"/>
    </project>

    <project name="webgl-full">
      <prerequisite name="webgl" hardware="gen9,gen9_iris" shard="6"/>
    </project>
    
    <project name="reboot-slave"/>

    <project name="scons-buildtest" src_dir="mesa"/>

    <project name="clean-workspace"/>
    <!-- TODO: find a way to variablize this list of hardware -->
    <project name="clean-all-workspaces">
      <prerequisite name="clean-workspace" hardware="bdwgt2-01,bdwgt2-03,bdwgt2-04,bdwgt2-05,bdwgt2-06,bdwgt2-07,bdwgt3e-01,bdwgt3e-02,bdwgt3e-03,bdwgt3e-04,bdwgt3e-05,bdwgt3e-08,bsw-01,bsw-02,bsw-03,bsw-04,bsw-05,bsw-06,bsw-07,bsw-08,bxt-01,bxt-02,bxt-03,bxt-04,bxt-05,bxt-07,bxt-08,bxt-09,bxt-10,bxt-11,bxt-12,byt-01,byt-02,byt-09,byt-10,byt-11,byt-12,byt-13,byt-14,cfl-01,cfl-03,cfl-04,g33-01,g45-01,g45-02,g45-03,g45-04,g965-01,g965-03,g965-04,g965-05,g965-07,g965-08,glk-01,glk-02,glk-04,glk-05,glk-06,hswgt1-01,hswgt1-02,hswgt2-01,hswgt2-02,hswgt2-03,hswgt2-04,hswgt2-05,hswgt3e-01,hswgt3e-02,hswgt3e-03,hswgt3e-04,i915-01,i915-02,i915-03,ilk-01,ilk-02,ilk-03,ilk-04,ilk-05,ivbgt1-01,ivbgt1-02,ivbgt2-01,ivbgt2-02,ivbgt2-03,ivbgt2-04,kblgt2-01,kblgt3-01,kblgt3-02,kblgt3-03,kblgt3-04,kblgt3-05,kblr-01,kblr-02,sklgt2-02,sklgt2-03,sklgt2-04,sklgt2-05,sklgt2-07,sklgt2-08,sklgt2-09,sklgt4e-01,sklgt4e-02,sklgt4e-03,sklgt4e-04,snbgt1-01,snbgt1-02,snbgt1-03,snbgt1-04,snbgt1-05,snbgt2-01,snbgt2-02,snbgt2-03"/>
    </project>

  </projects>

  <!-- the following servers and remotes correspond to projects -->
  <repos>
    <mesa repo="git://github.com/mesa3d/mesa.git">
      <!-- todo: add support for remotes.  -->
      <remote name="ci_mesa_repo" repo="https://gitlab.freedesktop.org/Mesa_CI/repos/mesa.git"/>
      <remote name="jekstrand" repo="https://gitlab.freedesktop.org/jekstrand/mesa.git"/>
      <remote name="tpalli" repo="https://gitlab.freedesktop.org/tpalli/mesa.git"/>
      <remote name="aphogat" repo="git://github.com/aphogat/mesa"/>
      <remote name="dcbaker" repo="https://gitlab.freedesktop.org/dbaker/mesa.git"/>
      <remote name="tarceri" repo="https://gitlab.freedesktop.org/tarceri/mesa.git"/>
      <remote name="evelikov" repo="git://github.com/evelikov/mesa"/>
      <remote name="pnmanolova" repo="git://github.com/pnmanolova/mesa"/>
      <remote name="imirkin" repo="git://github.com/imirkin/mesa"/>
      <remote name="igalia" repo="git://github.com/Igalia/mesa"/>
      <remote name="igalia_release" repo="git://github.com/Igalia/release-mesa"/>
      <remote name="internal-gitlab" repo="ssh://git@gitlab.devtools.intel.com:29418/linux-gfx/mesa/mesa.git"/>
      <remote name="chadv" repo="git://github.com/chadversary/mesa"/>
      <!--<remote name="hakzsam" repo="git://people.freedesktop.org/~hakzsam/mesa"/>-->
      <remote name="daniels" repo="https://gitlab.freedesktop.org/daniels/mesa.git"/>
      <remote name="igalia_khronos" repo="git@gitlab.khronos.org:vulkan/mesa.git"/>
      <!--<remote name="grigorig" repo="git://people.freedesktop.org/~grigorig/mesa"/>-->
      <remote name="yogesh-marathe" repo="git://github.com/yogesh-marathe/external-mesa"/>
      <remote name="anholt" repo="https://gitlab.freedesktop.org/anholt/mesa.git"/>
      <remote name="eric_engestrom" repo="https://gitlab.freedesktop.org/eric/mesa"/>
      <remote name="cmarcelo-internal" repo="ssh://git@gitlab.devtools.intel.com:29418/cmdeoliv/mesa.git"/>
      <remote name="airlied" repo="https://gitlab.freedesktop.org/airlied/mesa.git"/>
      <remote name="robclark" repo="git://github.com/freedreno/mesa"/>
      <remote name="global_logic" repo="https://gitlab.freedesktop.org/GL/mesa.git"/>
      <remote name="karolherbst" repo="https://github.com/karolherbst/mesa.git"/>
      <remote name="cwabbott" repo="https://gitlab.freedesktop.org/cwabbott0/mesa.git"/>
      <remote name="pzanoni" repo="https://gitlab.freedesktop.org/pzanoni/mesa.git"/>
      <!-- Erik Faye-Lund erik.faye-lund@collabora.com -->
      <remote name="kusma" repo="https://gitlab.freedesktop.org/kusma/mesa"/>
      <!-- Gert Wollny gw.fossdev@gmail.com -->
      <remote name= "gerddie" repo="https://gitlab.freedesktop.org/gerddie/mesa"/>
      <remote name= "ibriano" repo="https://gitlab.freedesktop.org/ibriano/mesa"/>
    </mesa>

    <drm repo="http://anongit.freedesktop.org/git/mesa/drm.git">
      <remote name="aphogat" repo="git://github.com/aphogat/drm"/>
    </drm>

    <vkrunner repo="git://github.com/Igalia/vkrunner.git" branch="origin/master"/>

    <piglit repo="git://github.com/mesa3d/piglit.git">
      <remote name="jekstrand" repo="https://gitlab.freedesktop.org/jekstrand/piglit.git"/>
      <!-- Dylan's repository is more stable than the origin, and will
           not trigger builds unnecessarily -->
      <remote name="dcbaker" repo="https://gitlab.freedesktop.org/dbaker/piglit.git"/>
      <remote name="nchery-gitlab" repo="https://gitlab.freedesktop.org/nchery/piglit.git"/>
      <!--<remote name="curro" repo="git://people.freedesktop.org/~currojerez/piglit"/>-->
      <!--<remote name="idr" repo="git://people.freedesktop.org/~idr/piglit"/>-->
      <!--<remote name="kwg" repo="git://people.freedesktop.org/~kwg/piglit"/>-->
      <!--<remote name="jljusten" repo="git://people.freedesktop.org/~jljusten/piglit"/>-->
      <!--<remote name="mattst88" repo="git://people.freedesktop.org/~mattst88/piglit"/>-->
      <remote name="antognolli" repo="https://gitlab.freedesktop.org/rantogno/piglit.git"/>
      <!--<remote name="tpalli" repo="git://people.freedesktop.org/~tpalli/piglit"/>-->
      <remote name="eric_engestrom" repo="https://gitlab.freedesktop.org/eric/piglit"/>
    </piglit>

    <waffle repo="https://gitlab.freedesktop.org/mesa/waffle.git">
      <remote name="jekstrand" repo="git://github.com/jekstrand/waffle"/>
    </waffle>

    <!-- just so the master can cache it for the builders -->
    <mesa_jenkins repo="https://gitlab.freedesktop.org/Mesa_CI/mesa_jenkins.git">
      <remote name="craftyguy" repo="https://gitlab.freedesktop.org/craftyguy/mesa_jenkins.git"/>
      <remote name="janesma" repo="https://gitlab.freedesktop.org/majanes/mesa_jenkins.git"/>
      <remote name="dcbaker" repo="https://github.com/dcbaker/mesa_jenkins.git"/>
      <remote name="ngcortes" repo="https://gitlab.freedesktop.org/ngcortes/mesa_jenkins.git"/>
    </mesa_jenkins>

    <mesa_ci repo="https://gitlab.freedesktop.org/Mesa_CI/mesa_ci.git">
      <remote name="craftyguy" repo="https://gitlab.freedesktop.org/craftyguy/mesa_ci.git"/>
      <remote name="dcbaker" repo="git://github.com/dcbaker/mesa_ci.git"/>
      <remote name="janesma" repo="https://gitlab.freedesktop.org/majanes/mesa_ci.git"/>
      <remote name="ngcortes" repo="https://gitlab.freedesktop.org/ngcortes/mesa_ci.git"/>
    </mesa_ci>

    <deqp repo="git://github.com/janesma/deqp.git"
          branch="aosp/master">
      <remote name="aosp" repo="https://android.googlesource.com/platform/external/deqp"/>
    </deqp>

    <cts repo="https://github.com/KhronosGroup/VK-GL-CTS.git"
         branch="origin/opengl-es-cts-3.2.5">
    </cts>

    <glcts repo="git@gitlab.khronos.org:Tracker/vk-gl-cts.git"
           branch="origin/opengl-cts-4.6.0">
    </glcts>

    <gtest repo="https://android.googlesource.com/platform/external/googletest"/>
    <glslang repo="git://github.com/KhronosGroup/glslang.git">
      <remote name="glsl" repo="git@gitlab.khronos.org:GLSL/glslang.git"/>
    </glslang>

    <crucible repo="https://gitlab.freedesktop.org/mesa/crucible.git"
              branch="origin/master">
      <remote name="eric_engestrom" repo="https://gitlab.freedesktop.org/eric/crucible"/>
    </crucible>

    <vulkancts repo="https://github.com/KhronosGroup/VK-GL-CTS.git"
               branch="origin/vulkan-cts-1.1.4">
      <remote name="khronos" repo="git@gitlab.khronos.org:Tracker/vk-gl-cts.git"/>
      <remote name="tpalli" repo="ssh://git@gitlab.devtools.intel.com:29418/tpalli/vk-gl-cts-tpalli.git"/>
    </vulkancts>
    
    <spirvtools repo="git://github.com/KhronosGroup/SPIRV-Tools.git"
                branch="khronos/master">
      <remote name="khronos" repo="ssh://git@gitlab.khronos.org/spirv/spirv-tools.git"/>
    </spirvtools>
    
    <spirvheaders repo="git://github.com/KhronosGroup/SPIRV-Headers"
                  branch="origin/master">
      <remote name="khronos" repo="git@gitlab.khronos.org:spirv/SPIRV-Headers.git"/>
    </spirvheaders>
    
    <kc-cts repo="git@gitlab.khronos.org:opengl/kc-cts.git"/>

    <mesa_ci_internal repo="ssh://git@gitlab.devtools.intel.com:29418/mesa_ci/mesa_ci_internal.git"/>

    <sim-drm repo="ssh://git@gitlab.devtools.intel.com:29418/mesa/sim-drm.git">
      <remote name="craftyguy" repo="git://github.intel.com/cacraft/sim-drm.git"/>
    </sim-drm>

    <skqp repo="https://skia.googlesource.com/skia"
      branch="2441c92442">
    </skqp>

    <!-- The following are dependencies for SKQP -->
    <google_buildtools repo="https://chromium.googlesource.com/chromium/buildtools.git" />
    <google_common repo="https://skia.googlesource.com/common.git" />
    <angle2 repo="https://chromium.googlesource.com/angle/angle.git" />
    <dng_sdk repo="https://android.googlesource.com/platform/external/dng_sdk.git" />
    <expat repo="https://android.googlesource.com/platform/external/expat.git" />
    <freetype repo="https://skia.googlesource.com/third_party/freetype2.git" />
    <harfbuzz repo="https://skia.googlesource.com/third_party/harfbuzz.git" />
    <icu repo="https://chromium.googlesource.com/chromium/deps/icu.git" />
    <imgui repo="https://skia.googlesource.com/external/github.com/ocornut/imgui.git" />
    <jsoncpp repo="https://chromium.googlesource.com/external/github.com/open-source-parsers/jsoncpp.git" />
    <libjpeg-turbo repo="https://skia.googlesource.com/external/github.com/libjpeg-turbo/libjpeg-turbo.git" />
    <libpng repo="https://skia.googlesource.com/third_party/libpng.git" />
    <libwebp repo="https://chromium.googlesource.com/webm/libwebp.git" />
    <lua repo="https://skia.googlesource.com/external/github.com/lua/lua.git" />
    <microhttpd repo="https://android.googlesource.com/platform/external/libmicrohttpd" />
    <piex repo="https://android.googlesource.com/platform/external/piex.git" />
    <sdl repo="https://skia.googlesource.com/third_party/sdl" />
    <sfntly repo="https://chromium.googlesource.com/external/github.com/googlei18n/sfntly.git" />
    <skcms repo="https://skia.googlesource.com/skcms" />
    <zlib repo="https://chromium.googlesource.com/chromium/src/third_party/zlib" />
    <skqp_assets repo="https://gitlab.freedesktop.org/Mesa_CI/skqp_assets.git" />

    <webgl repo="https://github.com/KhronosGroup/WebGL.git"/>
    <amber repo="https://github.com/google/amber.git"/>
  </repos>


  <branches>
    <!-- the following branches are polled continuously.  Any commit
         will trigger a branch build with an identifier based on the
         commit that triggered the build.  Any repository listed as a
         subtag of the branch can trigger a build of the branch.
         Repositories default to origin/master -->

    <!-- jenkins has a build with same name as branch -->
    <branch name="mesa_master" project="all-test">

      <!-- these repo tags exist soley to trigger a master build when
           anything changes -->
      <mesa/>
      <piglit/>
      <waffle/>
      <drm/>
      <vkrunner/>
      <deqp branch="aosp/master"/>
      <cts branch="origin/opengl-es-cts-3.2.5"/>
      <!-- skqp is pinned since newer versions provide completely different
           interfaces for running/results -->
      <skqp branch="2441c92442"/>
      <webgl/>

      <!-- the trigger attribute is used specify a branch that should
           be used, but prevent builds from triggering if the branch
           changes.  In this case, the prerelease repo contains test
           status for unreleased platforms.  We want to use the
           latest, but we don't want to retest every time a config
           item changes. -->
      <prerelease trigger="false"/>
    </branch>

    <branch name="mesa_19.2" project="all-test-vulkan">
      <mesa branch="origin/19.2"/>
      <cts branch="e67539f8f"/>
      <deqp branch="8804d6b51"/>
      <drm branch="14922551"/>
      <glcts branch="9d382ce55"/>
      <piglit branch="b9c7f59d3a"/>
      <skqp branch="2441c92442"/>
      <vkrunner branch="501009d"/>
      <waffle branch="1ded029"/>
      <spirvtools branch="f2803c4a"/>
      <spirvheaders branch="c4f8f65"/>
      <webgl branch="e2aa68814"/>
      <vulkancts branch="3064c5ca7"/>
      <crucible branch="e7aca1a"/>
    </branch>

    <branch name="mesa_19.2_staging" project="all-test-vulkan">
      <mesa branch="origin/staging/19.2"/>
      <cts branch="e67539f8f"/>
      <deqp branch="8804d6b51"/>
      <drm branch="14922551"/>
      <glcts branch="9d382ce55"/>
      <piglit branch="b9c7f59d3a"/>
      <skqp branch="2441c92442"/>
      <vkrunner branch="501009d"/>
      <waffle branch="1ded029"/>
      <spirvtools branch="f2803c4a"/>
      <spirvheaders branch="c4f8f65"/>
      <webgl branch="e2aa68814"/>
      <vulkancts branch="3064c5ca7"/>
      <crucible branch="e7aca1a"/>
    </branch>

    <branch name="mesa_19.1" project="all-test-vulkan">
      <mesa branch="origin/19.1"/>
      <cts branch="afb0b4b17"/>
      <deqp branch="699d81d0b"/>
      <drm branch="6a7d1329"/>
      <glcts branch="60a7f1b15"/>
      <piglit branch="0105d9f43f"/>
      <skqp branch="2441c92442"/>
      <vkrunner branch="770cb64"/>
      <waffle branch="1ded029"/>
      <spirvtools branch="0e68bb36"/>
      <spirvheaders branch="a2c529b"/>
      <webgl branch="e2aa68814"/>
    </branch>

    <branch name="mesa_19.1_staging" project="all-test-vulkan">
      <mesa branch="origin/staging/19.1"/>
      <cts branch="afb0b4b17"/>
      <deqp branch="699d81d0b"/>
      <drm branch="6a7d1329"/>
      <glcts branch="60a7f1b15"/>
      <piglit branch="0105d9f43f"/>
      <skqp branch="2441c92442"/>
      <vkrunner branch="770cb64"/>
      <waffle branch="1ded029"/>
      <spirvtools branch="0e68bb36"/>
      <spirvheaders branch="a2c529b"/>
      <webgl branch="e2aa68814"/>
    </branch>

    <branch name="vulkancts" project="vulkancts-full">
      <mesa/>
      <vulkancts/>
      <crucible/>
      <glslang/>
      <spirvheaders branch="801cca8104245c07e8cc53292da87ee1b76946fe"/>
      <spirvtools/>
    </branch>

    <branch name="jekstrand_gl" project="test-single-arch">
      <mesa branch="jekstrand/jenkins_gl"/>
    </branch>

    <branch name="jekstrand_vulkan" project="vulkancts-full">
      <mesa branch="jekstrand/jenkins_vulkan"/>
    </branch>

    <branch name="jljusten_internal" project="test-single-arch">
      <mesa branch="internal-gitlab/jljusten/jenkins/internal"/>
    </branch>

    <branch name="jljusten_tgl" project="test-single-arch-tgl">
      <mesa branch="internal-gitlab/jljusten/jenkins/tgl"/>
    </branch>

    <branch name="kwg" project="test-single-arch">
      <mesa branch="ci_mesa_repo/dev/kwg"/>
    </branch>

    <branch name="antognolli" project="test-single-arch">
      <mesa branch="ci_mesa_repo/dev/antognolli"/>
    </branch>

    <branch name="antognolli_tgl" project="test-single-arch-tgl">
      <mesa branch="internal-gitlab/rantogno/tgl/jenkins"/>
    </branch>

    <branch name="antognolli_vulkan" project="vulkancts-full">
      <mesa branch="ci_mesa_repo/dev/antognolli_vulkan"/>
    </branch>

    <branch name="antognolli_iris" project="all-test-iris">
      <mesa branch="ci_mesa_repo/dev/antognolli_iris"/>
    </branch>

    <branch name="kwg_vulkan" project="vulkancts-full">
      <mesa branch="ci_mesa_repo/dev/kwg_vulkan"/>
    </branch>

    <branch name="idr" project="test-single-arch-vulkan">
      <mesa branch="ci_mesa_repo/dev/idr/jenkins"/>
    </branch>

    <branch name="mattst88" project="test-single-arch-vulkan">
      <mesa branch="ci_mesa_repo/dev/mattst88"/>
    </branch>

    <branch name="majanes" project="test-single-arch-vulkan">
      <mesa branch="ci_mesa_repo/dev/janesma"/>
    </branch>

    <branch name="dcbaker" project="test-single-arch">
      <piglit branch="dcbaker/jenkins"/>
    </branch>

    <branch name="dcbaker_mesa" project="test-single-arch">
      <mesa branch="dcbaker/jenkins_gl"/>
    </branch>

    <branch name="jljusten" project="test-single-arch">
      <mesa branch="ci_mesa_repo/dev/jljusten"/>
    </branch>

    <branch name="tpalli" project="test-single-arch-vulkan">
      <mesa branch="tpalli/jenkins"/>
    </branch>

    <branch name="tpalli_vulkancts" project="vulkancts-full">
      <mesa branch="tpalli/jenkins-vkglcts"/>
      <vulkancts branch="tpalli/master"/>
    </branch>

    <branch name="curro" project="test-single-arch">
      <mesa branch="ci_mesa_repo/dev/curro"/>
    </branch>

    <branch name="curro_vulkan" project="vulkancts-full">
      <mesa branch="ci_mesa_repo/dev/curro-vk"/>
    </branch>

    <branch name="aphogat" project="test-single-arch">
      <mesa branch="aphogat/jenkins"/>
    </branch>

    <branch name="aphogat_vulkan" project="vulkancts-full">
      <mesa branch="aphogat/jenkins-vulkan"/>
    </branch>

    <branch name="nchery_vulkan" project="vulkancts-full">
      <mesa branch="ci_mesa_repo/dev/nchery_vulkan"/>
    </branch>

    <branch name="nchery" project="test-single-arch-nchery">
      <mesa branch="ci_mesa_repo/dev/nchery"/>
    </branch>

    <branch name="nchery_tgl" project="test-single-arch-tgl">
      <mesa branch="internal-gitlab/nchery/wip/tgl"/>
    </branch>

    <branch name="nchery_tgl-sim" project="test-single-arch-tgl-sim">
      <mesa branch="internal-gitlab/nchery/wip/tgl-simdrm"/>
    </branch>

    <branch name="tarceri" project="test-single-arch-vulkan">
      <mesa branch="tarceri/intel_ci"/>
    </branch>

    <branch name="evelikov" project="test-single-arch-vulkan">
      <mesa branch="evelikov/intel-ci"/>
    </branch>

    <!-- These branches exists to separate build results from
         mesa_master on the server. -->
    <branch name="nir_torture_test" project="test-single-arch"/>
    <branch name="shader_cache_test" project="test-single-arch"/>
    <branch name="mesa_custom" project="all-test"/>
    <branch name="deqp-runtime_test" project="test-single-arch"/>

    <branch name="pnmanolova" project="test-single-arch">
      <mesa branch="pnmanolova/jenkins"/>
    </branch>

    <branch name="imirkin" project="test-single-arch">
      <mesa branch="imirkin/jenkins"/>
    </branch>

    <branch name="djdeath_vulkan" project="vulkancts-full">
      <mesa branch="ci_mesa_repo/dev/djdeath_vulkan"/>
    </branch>

    <branch name="djdeath_vulkan_internal" project="vulkancts-full">
      <mesa branch="internal-gitlab/dev/djdeath_vulkan"/>
    </branch>

    <branch name="djdeath" project="test-single-arch">
      <mesa branch="ci_mesa_repo/dev/djdeath"/>
    </branch>

    <branch name="igalia" project="test-single-arch-vulkan">
      <mesa branch="igalia/jenkins"/>
    </branch>

    <branch name="igalia_khronos" project="test-single-arch-vulkan">
      <mesa branch="igalia_khronos/wip/igalia/jenkins-khronos"/>
    </branch>

    <branch name="jmcasanova" project="test-single-arch-vulkan">
      <mesa branch="igalia/jmcasanova/jenkins"/>
    </branch>

    <branch name="itoral" project="test-single-arch-vulkan">
      <mesa branch="igalia/itoral/jenkins"/>
    </branch>

    <branch name="agomez" project="test-single-arch-vulkan">
      <mesa branch="igalia/wip/agomez/ci"/>
    </branch>

    <branch name="siglesias" project="test-single-arch-vulkan">
      <mesa branch="igalia/siglesias/jenkins"/>
    </branch>

    <branch name="apinheiro" project="test-single-arch-vulkan">
      <mesa branch="igalia/apinheiro/jenkins"/>
    </branch>

    <branch name="apuentes" project="test-single-arch-vulkan">
      <mesa branch="igalia/apuentes/jenkins"/>
    </branch>

    <branch name="jasuarez" project="test-single-arch-vulkan">
      <mesa branch="igalia/jasuarez/jenkins"/>
    </branch>

    <branch name="elima" project="test-single-arch-vulkan">
      <mesa branch="igalia/elima/jenkins"/>
    </branch>

    <branch name="estea" project="test-single-arch-vulkan">
      <mesa branch="igalia/hikiko/jenkins"/>
    </branch>

    <branch name="internal" project="test-single-arch">
      <mesa branch="internal-gitlab/internal"/>
    </branch>

    <branch name="chadv" project="test-single-arch-vulkan">
      <mesa branch="chadv/jenkins"/>
    </branch>

    <branch name="hakzsam" project="test-single-arch-vulkan">
      <mesa branch="hakzsam/jenkins"/>
    </branch>

    <branch name="daniels" project="test-single-arch-vulkan">
      <mesa branch="daniels/ci/intel"/>
    </branch>
    
    <branch name="cmarcelo_gl" project="test-single-arch">
      <mesa branch="ci_mesa_repo/dev/cmarcelo_gl"/>
    </branch>

    <branch name="cmarcelo_vulkan" project="vulkancts-full">
      <mesa branch="ci_mesa_repo/dev/cmarcelo_vulkan"/>
    </branch>

    <branch name="cmarcelo_tgl" project="test-single-arch-tgl">
      <mesa branch="cmarcelo-internal/jenkins/tgl"/>
    </branch>

    <branch name="cmarcelo_iris" project="all-test-iris">
      <mesa branch="ci_mesa_repo/dev/cmarcelo_iris"/>
    </branch>

    <branch name="sagarghuge" project="test-single-arch-vulkan">
      <mesa branch="ci_mesa_repo/dev/sagarghuge"/>
    </branch>

    <branch name="sagarghuge-tgl" project="test-single-arch-tgl">
      <mesa branch="internal-gitlab/sghuge/jenkins/tgl"/>
    </branch>

    <branch name="sagarghuge-tgl-sim" project="test-single-arch-tgl-sim">
      <mesa branch="internal-gitlab/sghuge/jenkins/tgl-simdrm"/>
    </branch>

    <branch name="sagarghuge_iris" project="all-test-iris">
      <mesa branch="ci_mesa_repo/dev/sagarghuge_iris"/>
    </branch>

    <branch name="yogesh-marathe" project="test-single-arch-vulkan">
      <mesa branch="yogesh-marathe/jenkins"/>
    </branch>

    <branch name="anholt" project="test-single-arch-anholt">
      <mesa branch="anholt/jenkins"/>
    </branch>

    <branch name="eric_engestrom" project="test-single-arch-vulkan">
      <mesa branch="eric_engestrom/jenkins"/>
    </branch>

    <branch name="nroberts" project="test-single-arch-vulkan">
      <mesa branch="igalia/nroberts/jenkins"/>
    </branch>

    <branch name="frohlich" project="test-single-arch-vulkan">
      <mesa branch="ci_mesa_repo/dev/frohlich"/>
    </branch>

    <branch name="airlied" project="test-single-arch-vulkan">
      <mesa branch="airlied/jenkins"/>
    </branch>

    <branch name="robclark" project="test-single-arch-vulkan">
      <mesa branch="robclark/for_jenkins"/>
    </branch>

    <branch name="global_logic" project="test-single-arch-vulkan">
      <mesa branch="global_logic/i965_ci"/>
    </branch>

    <branch name="global_logic_2" project="test-single-arch-vulkan">
      <mesa branch="global_logic/i965_second"/>
    </branch>

    <branch name="karolherbst" project="test-single-arch-vulkan">
      <mesa branch="karolherbst/jenkins"/>
    </branch>

    <branch name="cwabbott" project="test-single-arch-vulkan">
      <mesa branch="cwabbott/intel-jenkins"/>
    </branch>
    
    <branch name="kusma" project="test-single-arch-vulkan">
      <mesa branch="kusma/intel-ci"/>
    </branch>
    
    <branch name="gerddie" project="test-single-arch-vulkan">
      <mesa branch="gerddie/intel-ci"/>
    </branch>

    <branch name="craftyguy" project="test-single-arch-vulkan">
      <mesa branch="ci_mesa_repo/dev/craftyguy"/>
    </branch>

    <branch name="agoldmints" project="test-single-arch-vulkan">
      <mesa branch="igalia/agoldmints/jenkins"/>
    </branch>

    <branch name="pzanoni" project="test-single-arch-vulkan">
      <mesa branch="pzanoni/for-ci"/>
    </branch>

    <branch name="ibriano" project="test-single-arch-vulkan">
      <mesa branch="ibriano/leeroy"/>
    </branch>

    <branch name="pendingchaos" project="test-single-arch-vulkan">
      <mesa branch="ci_mesa_repo/dev/pendingchaos"/>
    </branch>

    <branch name="internal_tgl" project="test-single-arch-tgl">
      <mesa branch="internal-gitlab/jljusten/wip/tgl"/>
    </branch>

    <branch name="simdrm_tgl" project="test-single-arch-tgl-sim">
      <mesa branch="internal-gitlab/jljusten/wip/tgl"/>
    </branch>

    <branch name="pnmanolova_tgl" project="test-single-arch-tgl">
      <mesa branch="internal-gitlab/pnmanolova/jenkins"/>
    </branch>

  </branches>

</build_specification>
